steps = [
    [
        # Create a table
        """
        CREATE TABLE users (
            id SERIAL PRIMARY KEY UNIQUE,
            username VARCHAR(255) NOT NULL,
            hashed_password VARCHAR(255) NOT NULL,
            email VARCHAR(255) NOT NULL UNIQUE,
        );
        """,
        # Drop the table
        """
        DROP TABLE users;
        """,
    ],
    [
        """
        CREATE TABLE spirits (
            id SERIAL PRIMARY KEY UNIQUE,
            type VARCHAR(255) NOT NULL,
            brand VARCHAR(255) NOT NULL,
            name VARCHAR(255) NOT NULL,
            picture_url VARCHAR(255),
            description VARCHAR(255),
            owner INTEGER NOT NULL REFERENCES users("id") ON DELETE CASCADE,
        );
        """,
        # Drop the table
        """
        DROP TABLE spirits;
        """,
    ],
]
