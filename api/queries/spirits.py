import os
from psycopg_pool import ConnectionPool
from pydantic import BaseModel
from typing import Optional, List, Union


pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


class Error(BaseModel):
    message: str


class DuplicateSpiritError(ValueError):
    pass


class SpiritIn(BaseModel):
    type: str
    brand: str
    name: str
    picture_url: Optional[str]
    description: Optional[str]


class SpiritOut(BaseModel):
    id: int
    type: str
    brand: str
    name: str
    owner: int
    picture_url: Optional[str]
    description: Optional[str]


class SpiritRepository(BaseModel):
    def spirit_in_to_out(self, id: int, owner: int, spirit: SpiritIn):
        old_data = spirit.dict()
        return SpiritOut(id=id, owner=owner, **old_data)

    def record_to_spirit_out(self, record):
        return SpiritOut(
            id=record[0],
            type=record[1],
            brand=record[2],
            name=record[3],
            picture_url=record[4],
            description=record[5],
            owner=record[6],
        )

    def create(self, account_id: int, spirit: SpiritIn) -> SpiritOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO spirits
                        (type, brand, name, owner, picture_url, description)
                        VALUES
                        (%s, %s, %s, %s, %s, %s)
                        RETURNING id;
                    """,
                    [
                        spirit.type,
                        spirit.brand,
                        spirit.name,
                        account_id,
                        spirit.picture_url,
                        spirit.description,
                    ],
                )
                id = result.fetchone()[0]
                return self.spirit_in_to_out(id, account_id, spirit)

    def get_spirits(self, account_id: int) -> Union[Error, List[SpiritOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                        id, type, brand, name, owner, picture_url, description
                        FROM spirits
                        WHERE owner = %s
                        ORDER BY id;
                        """,
                        [account_id],
                    )
                    result = []
                    for record in db:
                        spirit = SpiritOut(
                            id=record[0],
                            type=record[1],
                            brand=record[2],
                            name=record[3],
                            owner=record[4],
                            picture_url=record[5],
                            description=record[6],
                        )
                        result.append(spirit)
                    return result
        except Exception:
            return {"message": "Could not get all spirits"}

    def get_spirit(self, spirit_id: int) -> Optional[SpiritOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM spirits
                        WHERE id = %s
                        """,
                        [spirit_id],
                    )
                    record = result.fetchone()
                    return self.record_to_spirit_out(record)
        except Exception:
            return {"message": "Could not get that spirit"}

    def update_spirit(
        self, account_id: int, spirit_id: int, spirit: SpiritIn
    ) -> Union[SpiritOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                            UPDATE spirits
                            SET type = %s
                             , brand = %s
                             , name = %s
                             , owner = %s
                             , picture_url = %s
                             , description = %s
                             WHERE id = %s
                             RETURNING *
                        """,
                        [
                            spirit.type,
                            spirit.brand,
                            spirit.name,
                            account_id,
                            spirit.picture_url,
                            spirit.description,
                            spirit_id,
                        ],
                    )
                    record = result.fetchone()
                    return self.record_to_spirit_out(record)

        except Exception:
            return {"message": "Could not update that spirit"}

    def delete_spirit(self, spirit_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM spirits
                        WHERE id = %s
                        """,
                        [spirit_id],
                    )
                    return True
        except Exception:
            return False
