import os
from psycopg_pool import ConnectionPool
from pydantic import BaseModel

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


class DuplicateAccountError(ValueError):
    pass


class AccountIn(BaseModel):
    username: str
    password: str
    email: str


class AccountOut(BaseModel):
    id: str
    username: str
    email: str


class CookieAccountOut(BaseModel):
    id: str
    username: str
    hashed_password: str
    email: str


class AccountOutWithPassword(AccountOut):
    hashed_password: str


class AccountQueries:
    def record_to_account_out(self, record) -> AccountOutWithPassword:
        account_dict = {
            "user_id": record[0],
            "username": record[1],
            "email": record[2],
            "hashed_password": record[3],
        }
        return account_dict

    def create(
        self, user: AccountIn, hashed_password: str
    ) -> AccountOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO users
                            (username, email, hashed_password)
                        VALUES
                            (%s, %s, %s)
                        RETURNING
                        id, username, email, hashed_password;
                        """,
                        [
                            user.username,
                            user.email,
                            hashed_password,
                        ],
                    )
                    record = None
                    row = result.fetchone()
                    print(row)
                    if row is not None:
                        record = {}
                        for i, column in enumerate(db.description):
                            record[column.name] = row[i]
                    else:
                        return None
                    return AccountOutWithPassword(
                        id=row[0],
                        username=row[1],
                        email=row[2],
                        hashed_password=row[3],
                    )
        except Exception as e:
            return {"message": f"{e}Could not create account"}

    def get(self, username: str) -> AccountOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                        id,
                        username,
                        email,
                        hashed_password

                        FROM users

                        WHERE username = %s
                        """,
                        [username],
                    )
                    record = None
                    row = db.fetchone()
                    if row is not None:
                        record = {}
                        for i, column in enumerate(db.description):
                            record[column.name] = row[i]
                    if record is None:
                        return None
                    return AccountOutWithPassword(
                        id=row[0],
                        username=row[1],
                        email=row[2],
                        hashed_password=row[3],
                    )
        except Exception as e:
            return {"message": f"{e}Could not get account"}

    def get_user(self, username: str) -> AccountOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                        id,
                        username,
                        email

                        FROM users

                        WHERE username = %s
                        """,
                        [username],
                    )
                    record = None
                    row = db.fetchone()
                    if row is not None:
                        record = {}
                        for i, column in enumerate(db.description):
                            record[column.name] = row[i]
                    if record is None:
                        return None
                    return AccountOut(
                        id=row[0],
                        username=row[1],
                        email=row[2],
                    )
        except Exception as e:
            return {"message": f"{e}Could not get account"}

    def update_user(self, username: str, user_data: AccountIn) -> AccountOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    UPDATE users
                    SET
                        username = %s,
                        email = %s,
                    WHERE username = %s
                    RETURNING *;
                    """,
                    [
                        user_data.username,
                        user_data.email,
                        username,
                    ],
                )
                row = result.fetchone()
                if row:
                    return AccountOut(
                        id=row[0],
                        username=row[1],
                        email=row[2],
                    )
                return None
